package seartipy.examples

import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue
import org.junit.Test as test

class QueueTest {
    @test
    fun addLast() {
        val queue = Queue()
        queue.addLast(100);
        assertEquals(1, queue.size())
        queue.addLast(200)
        queue.addLast(300)
        assertEquals(3, queue.size())

    }
    @test
    fun peek() {
        val queue = Queue()
        assertTrue {queue.isEmpty()}
        queue.addLast(100)
        assertEquals(100,queue.peek())
        queue.addLast(200)
        assertEquals(100,queue.peek())

    }
    @test
    fun removeFirst() {
        val queue = Queue()
        queue.addLast(100);
        assertEquals(1, queue.size())
        assertNotEquals(0, queue.size())
        queue.addLast(200)
        queue.addLast(300)
        assertEquals(3, queue.size())
        assertEquals(100, queue.removeFirst())
    }

    @test
    fun isEmpty(){
        val queue = Queue()
        assertTrue{queue.isEmpty()}

    }
}