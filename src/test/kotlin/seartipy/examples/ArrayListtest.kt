package seartipy.examples

import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue
import org.junit.Test as test

class ArrayListTest{
    @test
    fun add(){
        var list = ArrayList()
        list.add(10)
        assertEquals(1,list.size)
        list.add(20)
        list.add(30)
        assertEquals(3,list.size)
        assertEquals(listOf(10,20,30),list.toList())
        list.add(40)
        assertEquals(10,list.first)
        assertEquals(40,list.last)
        assertEquals(listOf(10,20,30,40),list.toList())
        assertEquals(4,list.size)
    }
    @test
    fun pop(){
        var list = ArrayList()
        assertNull(list.pop())
        assertTrue { list.isEmpty() }
        list.add(10)
        assertEquals(10,list.pop())
        assertEquals(0,list.size)
        list.add(20)
        list.add(10)
        list.add(30)
        assertEquals(20,list.first)
        assertEquals(30,list.last)
        assertEquals(listOf(20,10,30),list.toList())
        assertEquals(3,list.size)
        assertEquals(30,list.pop())
        assertEquals(listOf(20,10),list.toList())
        assertEquals(10,list.pop())
        assertEquals(1,list.size)
    }
    @test
    fun insertAt(){
        var list = ArrayList()
        list.insertAt(10,0)
        list.insertAt(20,1)
        assertEquals(listOf(10,20),list.toList())
        list.insertAt(100,1)
        assertEquals(listOf(10,100,20),list.toList())
        list.insertAt(200,0)
        assertEquals(listOf(200,10,100,20),list.toList())
        assertEquals(4,list.size)
        list.insertAt(30,0)
        assertEquals(30,list.first)
        assertEquals(listOf(30,200,10,100,20),list.toList())
        assertEquals(5,list.size)
    }
    @test
    fun get(){
        var list = ArrayList()
        list.add(10)
        list.add(20)
        assertEquals(listOf(10,20),list.toList())
        assertEquals(2,list.size)
        assertEquals(10,list[0])
        assertEquals(2,list.size)
        list.add(30)
        list.add(70)
        assertEquals(listOf(10,20,30,70),list.toList())
        assertEquals(70,list.pop())
        assertEquals(listOf(10,20,30),list.toList())
        assertEquals(3,list.size)
        assertEquals(10,list[0])
        assertEquals(30,list[2])
        assertEquals(3,list.size)
    }
    @test
    fun set(){
        var list = ArrayList()
        list.add(10)
        list.add(20)
        list.add(30)
        assertEquals(listOf(10,20,30),list.toList())
        assertEquals(3,list.size)
        list.set(0,50)
        assertEquals(listOf(50,20,30),list.toList())
        list.set(2,70)
        assertEquals(listOf(50,20,70),list.toList())
        assertEquals(3,list.size)
        assertEquals(70,list.pop())
        assertEquals(2,list.size)
        list.set(0,22)
        assertEquals(listOf(22,20),list.toList())

    }
    @test
    fun removeAt(){
        var list = ArrayList()
        list.add(10)
        list.add(20)
        list.add(30)
        assertEquals(10,list.removeAt(0))
        assertEquals(listOf(20,30),list.toList())
        assertEquals(2,list.size)
        assertEquals(30,list.pop())
        assertEquals(listOf(20),list.toList())
        assertEquals(1,list.size)
        assertEquals(20,list.removeAt(0))
        assertNull(list.pop())

    }
    @test
    fun toList(){
        var list = ArrayList()
        assertTrue { list.isEmpty() }
        list.add(10)
        list.add(20)
        assertEquals(listOf(10,20),list.toList())
        list.add(30)
        assertEquals(listOf(10,20,30),list.toList())
        assertEquals(3,list.size)
        list.insertAt(70,0)
        assertEquals(listOf(70,10,20,30),list.toList())
        assertEquals(30,list.pop())
        list.set(0,22)
        assertEquals(listOf(22,10,20),list.toList())
        assertEquals(3,list.size)

    }
}