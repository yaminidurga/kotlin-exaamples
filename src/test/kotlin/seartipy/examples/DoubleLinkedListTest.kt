package seartipy.examples

import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertNull
import kotlin.test.assertTrue
import org.junit.Test as test


class DoubleLinkedListTest{
    @test
    fun insertFirst(){
        var doublelink=DoubleLinkedList()
        assertTrue { doublelink.isEmpty() }
        doublelink.insertFirst(100)
        assertEquals(listOf(100),doublelink.toList())
        doublelink.insertFirst(200)
        doublelink.insertFirst(300)
        assertEquals(listOf(300,200,100),doublelink.toList())
        assertEquals(3, doublelink.length())

    }

    @test
    fun insertLast(){
        var doublelink=DoubleLinkedList()
        assertTrue { doublelink.isEmpty() }
        doublelink.insertLast(100)
        assertEquals(listOf(100),doublelink.toList())
        doublelink.insertLast(200)
        doublelink.insertLast(300)
        assertEquals(listOf(100,200,300),doublelink.toList())
        assertEquals(3, doublelink.length())

    }

    @test
    fun toList(){
        var doublelink = DoubleLinkedList()
        assertTrue { doublelink.isEmpty() }
        doublelink.insertLast(100)
        assertEquals(listOf(100),doublelink.toList())
        doublelink.insertLast(200)
        assertEquals(listOf(100,200),doublelink.toList())
        doublelink.insertFirst(300)
        assertEquals(listOf(300,100,200),doublelink.toList())
        assertEquals(3, doublelink.length())
    }

    @test
    fun removeFirst(){
        var doublelink = DoubleLinkedList()
        doublelink.insertFirst(100)
        doublelink.insertFirst(200)
        assertEquals(listOf(200,100),doublelink.toList())
        assertEquals(200,doublelink.removeFirst())
        assertEquals(100,doublelink.removeFirst())
        assertNull(doublelink.removeFirst())
        assertTrue { doublelink.isEmpty() }
        doublelink.insertFirst(300)
        doublelink.insertFirst(400)
        doublelink.insertFirst(500)
        assertEquals(500,doublelink.removeFirst())
        assertEquals(400,doublelink.removeFirst())
        assertEquals(1, doublelink.length())
    }

    @test
    fun removeLast(){
        var doublelink = DoubleLinkedList()
        assertTrue { doublelink.isEmpty() }
        doublelink.insertLast(100)
        doublelink.insertLast(200)
        doublelink.insertLast(300)
        assertEquals(listOf(100,200,300),doublelink.toList())
        assertEquals(300,doublelink.removeLast())
        assertEquals(200,doublelink.removeLast())
        assertEquals(100,doublelink.removeLast())
        assertNull(doublelink.removeLast())

    }

    @test
    fun find(){
        var doublelink = DoubleLinkedList()
        assertNull(doublelink.find(100))
        doublelink.insertLast(100)
        assertEquals(1, doublelink.length())
        doublelink.insertFirst(200)
        doublelink.insertLast(300)
        assertEquals(3, doublelink.length())
        doublelink.insertFirst(400)
        assertEquals(listOf(400,200,100,300),doublelink.toList())
        assertEquals(100,doublelink.find(100)?.data)
        assertEquals(4, doublelink.length())

    }
    @test
    fun nodeAt(){
        var doublelink = DoubleLinkedList()
        //assertFails { doublelink.nodeAt(0) }
        doublelink.insertFirst(100)
        doublelink.insertFirst(200)
        assertEquals(2, doublelink.length())
        doublelink.insertLast(300)
        assertEquals(listOf(200,100,300),doublelink.toList())
        assertEquals(3, doublelink.length())
        assertEquals(200,doublelink.nodeAt(0)?.data)
        assertEquals(300,doublelink.nodeAt(2)?.data)
    }

    @test
    fun insert(){
        var doublelink = DoubleLinkedList()
        doublelink.insertLast(10)
        doublelink.insertFirst(20)
        assertEquals(listOf(20,10),doublelink.toList())
        assertEquals(2, doublelink.length())
        doublelink.insert(30,doublelink.firstNode!!)
        assertEquals(listOf(30,20,10),doublelink.toList())
        assertEquals(3, doublelink.length())
        doublelink.insert(60,doublelink.find(10)!!)
        assertEquals(listOf(30,20,60,10),doublelink.toList())
        assertEquals(4, doublelink.length())
        doublelink.insert(15,doublelink.lastNode!!)
        assertEquals(listOf(30,20,60,15,10),doublelink.toList())
        doublelink.insert(55,doublelink.nodeAt(3)!!)
        assertEquals(listOf(30,20,60,55,15,10),doublelink.toList())
        assertEquals(6, doublelink.length())
        doublelink.insert(22,doublelink.find(10)!!)
        assertEquals(listOf(30,20,60,55,15,22,10),doublelink.toList())
        assertEquals(7, doublelink.length())

    }

    @test
    fun remove(){
        var doublelink = DoubleLinkedList()
        doublelink.insertLast(10)
        doublelink.insertFirst(20)
        doublelink.insertFirst(30)
        assertEquals(listOf(30,20,10),doublelink.toList())
        assertEquals(3, doublelink.length())
        assertEquals(20,doublelink.remove(doublelink.nodeAt(1)!!))
        assertEquals(listOf(30,10),doublelink.toList())
        doublelink.insertFirst(20)
        doublelink.insertFirst(40)
        assertEquals(listOf(40,20,30,10),doublelink.toList())
        assertEquals(4, doublelink.length())
        assertEquals(10,doublelink.remove(doublelink.find(10)!!))
        assertEquals(listOf(40,20,30),doublelink.toList())
        assertEquals(3, doublelink.length())
    }
}