package seartipy.examples

import kotlin.test.assertEquals
import org.junit.Test as test

class CombinatoricsTest {
    @test
    fun factorialTest() {
        assertEquals(1, factorial(0))
        assertEquals(1, factorial(1))
        assertEquals(120, factorial(5))
    }
}