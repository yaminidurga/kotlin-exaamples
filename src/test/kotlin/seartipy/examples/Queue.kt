package seartipy.examples


class Queue{
    private var head:Node?=null
    private var tail:Node?=null
    private var length:Int=0


    fun addLast(value:Int){
        var node=Node(value,null)
        var t=tail
        if(t!=null){
            t.next=node
            tail=null
        }
        else{
            head=node
            tail=node
        }
        length++
    }

    fun peek():Int?{
        var node = head
        if(node != null){
            return node.data
        }
        return null
    }

    fun removeFirst():Int?{
        var first=head
        if(first!=null){
            head =first.next
            length--
            return first.data

        }
        return null

    }

    fun isEmpty():Boolean = length==0
    fun size()=length
}

