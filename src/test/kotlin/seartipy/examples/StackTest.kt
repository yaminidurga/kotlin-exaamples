package seartipy.examples

import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue
import org.junit.Test as test

class StackTest {
    @test
    fun push() {
        val stack = Stack()
        stack.push(100);
        assertEquals(1, stack.length())
        assertNotEquals(0,stack.length())
        stack.push(200)
        stack.push(300)
        assertEquals(3,stack.length())

    }
    @test
    fun peek(){
        val stack=Stack()
        stack.push(100)
        assertEquals(100,stack.peek())
        stack.push(300)
        assertEquals(300,stack.peek())
        assertEquals(2, stack.length())
    }
    @test
    fun pop(){
        val stack = Stack()
        assertEquals(0, stack.length())
        stack.push(50)
        stack.push(100)
        stack.push(200)
        stack.push(400)
        assertEquals(400,stack.pop())
        assertEquals(200,stack.pop())
        assertNotEquals(50, stack.pop())
    }
    @test
    fun isEmpty(){
        val stack = Stack()
        assertTrue{stack.isEmpty() }

    }
    @test
    fun max(){
        val stack = Stack()
        stack.push(100)
        stack.push(300)
        stack.push(50)
        assertEquals(300,stack.max())
        assertEquals(3, stack.length())
        stack.push(500)
        assertEquals(500,stack.max())
        stack.push(150)
        assertNotEquals(150,stack.max())


    }

}