package seartipy.examples


class Stack {
    private var top: Node? = null
    private var size: Int = 0

    fun push(value: Int) {
        top = Node(value, top)
        size++
    }

    fun peek():Int?{
        var node = top
        if(node!=null){
            return node.data
        }
        return null
    }

    fun pop(): Int? {
        size--
        var node = top
        if (node != null) {
            top = node.next
            return node.data
        }
        return null
    }

    fun isEmpty(): Boolean = size == 0
    fun length(): Int = size

    fun max():Int? {
        var node = top
        if (node != null) {
            var max = node.data
            node = node.next
            while (node != null) {
                if (max < node.data)
                    max = node.data
                node = node.next
            }
            return max
        }
        return null
    }
}
