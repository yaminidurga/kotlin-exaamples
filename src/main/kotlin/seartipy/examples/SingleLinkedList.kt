package seartipy.examples

fun insertAfter(value:Int,node: SingleNode): SingleNode {
    var temp = SingleNode(value,node.next)
    node.next = temp
    return temp
}

fun removeAfter(node: SingleNode):SingleNode? {
    var temp = node.next
    node.next = node.next?.next
    return temp
}

fun createList(arr:Array<Int>):SingleNode? {
    if(arr.size == 0)
        return null
    else {
        var temp: SingleNode = SingleNode(arr[0], null)
        var list: SingleNode = temp
        for (i in 1 until arr.size) {
            list = insertAfter(arr[i], list)
        }
        return temp
    }
}

fun printList(list:SingleNode) {
    var temp: SingleNode? = list
    while(temp != null){
        print(temp.data)
        temp = temp.next
    }
}

fun sum(list:SingleNode):Int {
    var temp:SingleNode? = list
    var s = 0
    while(temp!= null){
        s += temp.data
        temp = temp.next
    }
    return s
}

fun length(node:SingleNode?):Int {
    var temp:SingleNode? = node
    var count = 0
    while(temp != null){
        count += 1
        temp = temp.next
    }
    return count
}


