package seartipy.examples

class DoubleLinkedList {
    private var head: DoubleNode? = null
    private var tail: DoubleNode? = null
    private var size = 0

    val firstNode: DoubleNode?
        get() = head
    val lastNode: DoubleNode?
        get() = tail
    val first: Int?
        get() = head?.data
    val last: Int?
        get() = tail?.data


    fun insertFirst(value: Int) {
        var newNode = DoubleNode(null, value, head)
        var first = head
        if (first == null) {
            head = newNode
            tail = newNode
        } else {
            first.prev = newNode
            head = newNode
        }

        ++size
    }

    fun insertLast(value: Int) {
        var newNode = DoubleNode(tail, value, null)
        var last = tail
        if (last == null) {
            head = newNode
            tail = newNode
        } else {
            last.next = newNode
            tail = newNode
        }
        size++
    }

    fun isEmpty(): Boolean = size == 0
    fun length(): Int = size


    fun toList(): MutableList<Int> {
        var list = mutableListOf<Int>()
        var t = head
        while (t != null) {
            list.add(t.data)
            t = t.next
        }
        return list
    }

    fun removeFirst(): Int? {
        var first = head
        if (first != null) {
            first.next?.prev = null
            var result = first.data
            head = first.next
            size--
            return result
        }
        head = null
        tail = null
        return null
    }

    fun removeLast(): Int? {
        var last = tail
        if (last != null) {
            last.prev?.next = null
            var result = last.data
            tail = last.prev
            size--
            return result
        }
        return null
    }

    fun find(value: Int): DoubleNode? {
        var temp = head
        while (temp != null) {
            if (temp.data == value) {
                return temp
            }
            temp = temp.next
        }
        return null
    }

    fun nodeAt(index: Int): DoubleNode? {
        var i = 0
        var temp = head
        while (temp != null && i < size) {
            if (i == index) {
                return temp
            }
            temp = temp.next
            i++
        }
        return null
    }

    fun insert(value: Int, at: DoubleNode): Unit {
        var newNode = DoubleNode(at.prev, value, at)
        at.prev?.next = newNode
        at.prev = newNode
        if (at == head)
            head = newNode
        size++
        }

    fun remove(at: DoubleNode): Int {
        var temp = at
        at.prev?.next = at.next
        at.next?.prev = at.prev
        size--
        if (at == head) {
            head = at.next
        }
        else if (at == tail) {
            tail = at.prev
        }
        return temp.data
    }

    fun insertAfter(value: Int, at: DoubleNode): Unit {
        var newNode = DoubleNode(at.next, value, at)
        at.next?.prev = newNode
        at.next = newNode
        if (at == head) {
            at.next = newNode
            size++
        }
        else if(at == tail) {
            at.next = null
            tail = newNode
        }
    }

    fun removeAfter(at:DoubleNode):Int{
        var temp = at
        if(at == head && at == tail){
            at.next = null
            at.prev = null
        }
        else if(at == tail){
            at.next = null
            tail = at.prev?.next
        }
        else{
            at.prev?.next = at.next
            at.next?.prev = at.prev
            size--
        }
        return temp.data
    }
}



