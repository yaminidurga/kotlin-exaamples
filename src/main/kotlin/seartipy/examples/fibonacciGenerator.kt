package seartipy.examples

fun main(args:Array<String>){
    var fg = FibonacciGenerator(5)
    while(fg.hasNext()){
        println(fg.next())
    }
}

class FibonacciGenerator{
    private var n:Int
    private var count:Int = 0
    private var lo:Int = 0
    private var hi:Int = 1
    constructor(n:Int){
        this.n = n
    }
    fun next():Int {
        while(true){
            var t =lo+hi
            ++count
            lo = hi
            hi = t
            return lo
        }
    }

    fun hasNext() = count < n
}