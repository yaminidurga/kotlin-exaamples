package seartipy.examples


class ArrayList{
    private var arr = IntArray(20)
    private var length = 0

    val size:Int
        get() = length

    val first:Int?
        get()=arr[0]

    val last:Int? get()=arr[length-1]

    fun isEmpty() = length == 0

    fun ensureCapacity(newCapacity:Int){
        if(arr.size >=newCapacity)
            return
        val temp = IntArray(Math.max(newCapacity,arr.size *2 +1))
        System.arraycopy(arr,0,temp,0,arr.size)
        arr = temp
    }

    fun add(value:Int):Unit{
        ensureCapacity(arr[length+1])
        arr[length] = value
        ++length
    }
    fun pop():Int?{
        if(length == 0)
            return null
        length--
        return arr[length]
    }

    operator fun get(index:Int):Int{
        require(index in 0 .. arr.size)
        return arr[index]
    }

    operator fun set(index:Int,value:Int){
        require(index in 0 until length)
        arr[index] = value
    }

    fun insertAt(value:Int,at:Int){
        ensureCapacity(arr[length + 1])
        for(i in length downTo at+1){
            arr[i]=arr[i-1]
        }
        arr[at] = value
        length++

        }
    fun removeAt(at:Int):Int?{
        var temp =arr[at]
        length--
        for(i in at..length){
            arr[i] = arr[i+1]
        }
        return temp
    }

    fun toList():List<Int>{
        var list = mutableListOf<Int>()
        for(i in 0 until length){
            list.add(arr[i])
        }
        return list
    }
}

